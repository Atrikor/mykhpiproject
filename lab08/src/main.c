/**
 * @mainpage
 * # Загальне завдання
 * 1. **Сформувати** функцію, яка буде шукати НСД двох чисел.
 *
 * 2. **Сформувати** функцію, яка бдуе знаходити кількість слів у реченні.
 *
 * @author Lytvynenko Denys
 * @date 21-dec-2021
 * @version 1.0
 */
 /**
 * @file main.c
 * @brief Файл, який викликає одну з функцій та продемострувати її роботу.
 *
 * @author Lytvynenko Denys
 * @date 21-dec-2021
 * @version 1.0
 */




#include <stdlib.h>
#include <time.h>
/**
 * @brief Знаходження НСД
 * 
 * @param a перше число
 * @param b друге число
 * @return результат gcd типу int
 */

int gcd(int a, int b) {
/*
*Знаходить НСД чисел за алгоритмом Евкліда,
*від більшого числа віднімємо менше, якщо виходить 0 то число a = числу b отже вони і є НСД
*Якщо вони не = 0, то більше число заміняємо на різницю та переходимо в початок циклу
*/
	int tmp;	
		while (a != b) {			

			if (a > b) {
				tmp = a;
				a = b;
				b = tmp;

			}
			b = b - a;
		}

		int gcd = a; 

		return gcd;
		}
/**
 * @brief Рахунок кількості слів у реченні.
 * 
 * @param str[100] масив, який містить у собі речення
 * @return результат, який містить кількість слів у реченні
 */

int word_count(char str[100]) {

/*
*Знаходимо кількість слів завдяки пропускам між ними, одразу рахуємо останнє слово,
*потім за умовою, якщо даний символ дорівнює пропуску і наступний не дорівнює, 
*то додаємо один
*/

	int result = 1; 

	for(int i = 0; str[i] != '\0'; i++){
		if(str[i] == ' ' && str[i+1] != ' ')
			result++;
	}

	return result;

}
/**
 * @brief Вибір функції для виконання
 * 
 * @details Перший аргумент - вибір функції, яку користувач хоче виконати (1-4):
 * - Для функції 1 є опція задати числа, з яких і буде шукатися НСД
 * - Для функції 2 є опція задати рядок, кількість слів якого буде рахуватися
 * 
 * @param argc кількість аргументів 
 * @param argv агрументи
 */



int main(int argc, char** argv) {

	srand(time(NULL));
	if(argv[1][0] == '1'){
		int a = (argc >= 3) ? atoi(argv[2]) : rand()%101+1;
		int b = (argc >= 4) ? atoi(argv[3]) : rand()%101+1;
		int gcd_res = gcd(a,b);
	} else { 
		if(argv[1][0] == '2'){
			char* str = argc >= 3 ? argv[2] : "wdadaw wdwadawd aaw awdaw daw dwa  wdawd awdawd awdawdawd wadw";
			int word_count_res = word_count(str);
		}
	}



	return 0;


	}



