//Програма, яка рахує кількість слів у рядку.

int main() {

	char str[100] = "eins zwei drei vier funt sechs sieben acht      neun";

	int result = 1; // відразу рахуємо останнє слово

	for(int i = 0; str[i] != '\0'; i++){
		if(str[i] == ' ' && str[i+1] != ' ')// якщо даний символ дорівнює пропуску і наступний не дорівнює, то додаємо один
			result++;
	}

	return 0;

}
