/*Литвиненко Денис Олександрович, гр КІТ-121а

Завдання 20: За данним радіусом r так командами "l" "s" "v" користувача обчислити
"l" - довжину кола
"s" - площу круга
"v" - об'єм кулі.
*/

#define PI 3.14

int main() {

	int r = 3; //радіус кола

	double result = 0;// результат який виведе програма в залежності від команди

	double l = 2*PI*r; //формула довжини кола

	double s = PI*r*r; //формула площі круга

	double v = (4/3)*PI*r*r*r; //формула об'єму кулі

	char command = 0; //змінна яка приймає значення команди

		switch(command) {     //розгалудження програми в залежності від введаної команди
			case 'l':
			 result = l;
			 break;
			case 's':
			 result = s;
			 break;
			case 'v':
			 result = v;
			 break;
			default:
			 result = 0;
			}
	return 0;

 }


