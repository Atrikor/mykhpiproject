**Литвиненко Денис**
**КІТ 121а**

Завдання 20: за даним радіусом 'r' та командами 'l' 's' 'v' користувача обчислити:

- довжину кола 'l' 

- площу круга 's'

- об'єм кулі 'v'
