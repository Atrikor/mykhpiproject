<<<<<<< HEAD
###Лабораторна робота №5. Цикли.
#Литвиненко Денис Олександрович, гр КІТ 121а
=======
### Литвиненко Денис Олександрович, гр КІТ 121а
>>>>>>> d5b14eff6bcebf7f09e51a83014bb4c2ba1298cf

**Завдання 22**: Визначити найбільший спільний дільник двох заданих чисел.

Точкою входу моєї програми є функція `main()`, вона знаходиться на початку і є початком моєї програми

**Опис роботи основної функціїї**:найбільший спільний дільний я шукав за алгоритмом Евкліда через віднімання. А саме:
	- Від більшого віднімаємо менше число
	- Якщо виходить `0` то число` а` = числу `b`  отже вони і є НСД
	- Якщо не 0, то більше число заміняємо на залишок 
	- Повторюємо 1 пункт

Цей алгоритм я написав таким чином: 

***

	while (a != b) {
	
			if (a > b) {
				tmp = a;
				a = b;
				b = tmp;

			}
			
			b = b - a;
		  }
		
***
- **Перелік вхідних данних** :
	- int a -перше число 
	- int b -друге число 
	- int tmp -залишок від віднімання
	
- **Дослідження результатів роботи програми**:

	- для підтвердження коректності роботи програми, зупинено відлагодник на рядку з `return 0` запустив програму та написав команду `p gcd` яка виводить змінну `gcd` яка і є НСД та  `fr v` яка виводить усі змінні. Після вводу командиотримав результат:
	
***

	   17  			int gcd = a; //наш результат, змінна НСД
	   18 
	-> 19  			return 0;
	   20  	 }
	(lldb) p gcd
	(int) $0 = 4
	(lldb) fr v
	(int) a = 4
	(int) b = 4
	(int) tmp = 8
	(int) gcd = 4

***

Структура директорії моєї лабораторної роботи:

***

	lab05
	├── doc
	│   └── lab04.md
	├── Makefile
	├── README.md
	└── src
	└── main.c
	
***

**Висновки**: при виконанні лабораторної роботи були набуті практичні навички зі створення циклічних конструкцій, програм на мові С, зокрема: обчислювати математичні вирази, написання математичних алгоритмів, а саме алгоритму Евкліда на мові С.
